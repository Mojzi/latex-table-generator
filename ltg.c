#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Table
{
    int columns, rows;
    int style;
    unsigned int *longestField;
    char *alignment;
    char **field;
};

struct Table *table = 0;

void deleteTable()
{
    if(table)
    {
        if(table->longestField) { free(table->longestField); }
        if(table->alignment) { free(table->alignment); }
        for(int i=0; i<(table->rows * table->columns); i++)
        {
            if(table->field[i]) { free(table->field[i]); }
        }
        if(table->field) { free(table->field); }
        if(table) { free(table); }
        table = 0;
    }
}

int makeNewTable(int rows, int columns, int style)
{
    if(table != NULL) { return 0; }
    table = malloc(sizeof(*table));
    table->columns = columns;
    table->rows = rows;
    table->style = style;
    if(!(table->field = malloc(sizeof(char*)*rows*columns)))
    {
        deleteTable(table);
        return 0;
    }
    if(!(table->longestField = calloc(rows, sizeof(unsigned int))))
    {
        deleteTable(table);
        return 0;
    }
    if(!(table->alignment = calloc(columns, sizeof(char))))
    {
        deleteTable(table);
        return 0;
    }
    for(int i=0; i<table->columns; i++)
    {
        table->alignment[i] = 'l';
    }

    /* Init all field to not write "(null)" if user prints empty field */
    for(int i=0; i<rows*columns; i++)
    {
        if(!(table->field[i] = calloc(1, sizeof(char))))
        {
            deleteTable(table);
            return 0;
        }

    }

    for(int i=0; i<columns; i++)
    {
        table->longestField[i] = 0;
    }
    return 1;

};

int tableGetRowNum()
{
    return table->rows;
}
int tableGetColNum()
{
    return table->columns;
}

int tableIsInBounds(int row, int column)
{
    if(!table) { return 0; }
    if((table->rows <= row) || (table->columns <=column) || (row<0) || (column<0)) { return 0; }
    else { return 1; }
}

void tableWriteToField(int row, int column, const char *text)
{
    if(!table) { return; }
    if(!tableIsInBounds(row, column)) { return; }
    int index = table->rows * column + row;
    if(table->field[index]) { free(table->field[index]); }
    table->field[index] = malloc(sizeof(char) * (strlen(text)+1));

    //NOTE: Get length of the longest field in column for writing on console screen purpose
    if(strlen(text) > table->longestField[column])
    {
        table->longestField[column] = strlen(text);
    }
    else
    {
        table->longestField[column] = 0;
        for(int i=0;  i<table->rows;  i++)
        {
            if(strlen(table->field[i*column+row]) > table->longestField[column])
            {
                table->longestField[column] = strlen(table->field[i*column+row]);
            }
        }
    }
    strcpy(table->field[index], text);
}

char* tableReadFromField(int row, int column)
{
    if(!table) { return 0; }
    if(!tableIsInBounds(row, column)) { return 0; }
    int index = table->rows * column + row;
    static char *text = 0;
    text = malloc(strlen(table->field[index])+1);
    strcpy(text, table->field[index]);
    return text;
}

int tableGetFieldSize(int row, int column)
{
    if(!table) { return 0; }
    if(!tableIsInBounds(row, column)) { return 0; }
    int index = table->rows * column + row;
    return strlen(table->field[index]);
}

unsigned int tableLongestFieldSize(int column)
{
    return table->longestField[column];
}

int tableGetStyle()
{
    return table->style;
}

void tableSetStyle(int style)
{
    table->style = style;
}

char tableGetAlignment(int column)
{
    return table->alignment[column];
}

void tableSetAlignment(int column, int value)
{
    table->alignment[column] = value;
}

int tableExist()
{
    if(table) { return 1; }
    return 0;
}

int  writeTableToFile(const char* filename)
{
    FILE *fp = 0;
    if(!(fp = fopen(filename, "w")))
        return 0;

    //NOTE: Beginning and column settings
    int printLastOne = 0;
    fprintf(fp, "\\documentclass[a4paper]{article}\n");
    fprintf(fp, "\\begin{document}\n");
    fprintf(fp, "\\begin{tabular}{");
    for(int i=0; i<tableGetColNum(); i++)
    {
        switch(tableGetStyle())
        {
            case 1:
                break;
            case 2:
            case 3:
                if(i==0) break;
                fprintf(fp, "|");
                break;
            case 4:
            case 5:
                fprintf(fp, "|");
                if(i == tableGetColNum() - 1) { printLastOne = 1; }
                break;
            default:
                break;
        }
        fprintf(fp, "%c", tableGetAlignment(i));
        if(printLastOne)
        {
            fprintf(fp, "|");
            printLastOne = 0;
        }
    }
    fprintf(fp, "}");
    fprintf(fp, "\n");

    //NOTE: Fields and row settings
    for(int j=0; j<tableGetRowNum(); j++)
    {
        switch(tableGetStyle())
        {
            case 1:
            case 2:
                break;
            case 3:
                if(j==0) break;
                fprintf(fp, " \\hline");
                break;
            case 5:
                //NOTE: Go to case 4 if one of three cases is true
                if((j == -1)){}
                else if(j == 0){}
                else if(j == tableGetRowNum()-1){}
                else break;
            case 4:
                fprintf(fp, " \\hline");
                if(j == tableGetRowNum() - 1) { printLastOne = 1; }
                break;
            default:
                break;
        }
        fprintf(fp, "\n");
        for(int i=0; i<tableGetColNum(); i++)
        {
            fprintf(fp, tableReadFromField(j,i));
            if(i != tableGetColNum() - 1) fprintf(fp, " & ");
        }

        fprintf(fp, " \\\\");
        if(printLastOne)
        {
            fprintf(fp, " \\hline");
            printLastOne = 0;
        }
    }
    fprintf(fp, "\n\\end{tabular}\n");
    fprintf(fp, "\\end{document}\n");
    return 1;
}
