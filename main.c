#define _GNU_SOURCE /* fixes implicit getline() error */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ltg.h"


#define STYLE_NUMS 5

void printAvaliableStyles()
{
    const char *tableType[STYLE_NUMS] = 
    {
        "1 2 3\n       \n4 5 6\n       \n7 8 9",
        "1|2|3\n       \n4|5|6\n       \n7|8|9",
        "1|2|3\n ----- \n4|5|6\n ----- \n7|8|9",
        "-------\n|1|2|3| \n-------\n|4|5|6|\n-------\n|7|8|9|\n-------",
        "-------\n|1|2|3| \n-------\n|4|5|6|\n       \n|7|8|9|\n-------"
    };
    for(int i=0; i<STYLE_NUMS; i++)
    {
        printf("%d)##########\n\n%s\n\n",i+1, tableType[i]);
    }
}

//To get rid of newline character being passed as user input
void clearInputBuffer()
{
    char c;
    while((c =getchar()) != '\n' && (c != EOF));
}

void insertBorder(int position, const char border)
{
    if(border == 'h')
    {
        switch(tableGetStyle())
        {
            case 1:
                putchar(' ');
                break;
            case 2:
            case 3:
                if((position != tableGetColNum() - 1) && (position != -1) ) putchar('|');
                break;
            case 4:
            case 5:
                putchar('|');
                break;
            default:
                break;
        }
    }
    else if(border == 'v')
    {
        int length = 0;
        for (int i=0 ; i<tableGetColNum(); i++) { length += tableLongestFieldSize(i); }
        switch(tableGetStyle())
        {
            case 1:
            case 2:
                break;
            case 3:
                if(position == tableGetRowNum() - 1) break;
                if(position == -1) break;
                for(int i=0; i<length + tableGetColNum(); i++)
                    putchar('-');
                putchar('\n');
                break;
            case 5:
                //NOTE: Go to case 4 if one of three cases is true
                if( !( position == -1 || position == 0 || position == tableGetRowNum()-1 ) ) { break; }
            case 4:
                for(int i=0; i<length + tableGetColNum(); i++)
                    putchar('-');
                //if(position == 0) putchar('\n');
                putchar('\n');
                break;
            default:
                break;
        }
    }
}

void printTableOnScreen()
{
    insertBorder(-1, 'v');
    for(int j=0; j<tableGetRowNum(); j++)
    {
        insertBorder(-1, 'h');
        for(int i=0; i<tableGetColNum(); i++)
        {
            printf("%s", tableReadFromField(j,i));

            /* Make all field even in width */
            //unsigned int cl = strlen(tableReadFromField(i,j));
            unsigned int cl = tableGetFieldSize(j, i);
            if(cl < tableLongestFieldSize(i) )
            {
                for(unsigned int k=0; k<tableLongestFieldSize(i) - cl; k++) {putchar(' ');}
            }
            insertBorder(i, 'h');
        }
        printf("\n");
        insertBorder(j, 'v');
    }
    printf("\n");
}


void changeTableStyle(int style)
{
    if(style == 0)
    {
        printf("Wybierz styl tabeli (domyslnie 1): \n");
        printAvaliableStyles();
        printf("\nWybor: ");
        scanf("%d", &style);
        clearInputBuffer();
    }
    if((style < 1) || style > STYLE_NUMS) { style = 1; }
    tableSetStyle(style);
    printf("Wybrany styl: %d\n", tableGetStyle());
}


void createTable(int rows, int columns, int style)
{
    if(tableExist())
    {
        printf("Utworzenie nowej tablicy spowoduje usuniecie poprzedniej\n"
                "i utrate danych. Kontynuowac? (t/N):\n");
        char input = getchar();
        clearInputBuffer();
        if(input == 't' || input == 'T') { deleteTable(); }
        else return;
    }
    if( (rows == 0) || (columns == 0) )
    {
        printf("Podaj wymiary tabeli, ktora chcesz utworzyc:\n");
        scanf("%d %d", &columns, &rows);
        clearInputBuffer();
    }
    if(!(makeNewTable(rows, columns, style)))
    {
        printf("Nie udalo sie utworzyc tabeli o wymiarach %dx%d!\n", columns, rows);
        return;
    }
    else printf("Utworzono tabele o wymiarach %dx%d\n", columns, rows);
    changeTableStyle(style);
    system("clear");
    return;
}

void InsertFieldToTable()
{
    printf("Wpisz wierz i kolumne (numeracja od 1):\n");
    int x, y;
    char *text = 0;
    size_t len;
    scanf("%d %d", &y, &x);
    clearInputBuffer();
    if(!tableIsInBounds(x-1, y-1))
    {
        printf("Bledne wspolrzedne pola!\n");
        return;
    }
    printf("Wpisz zawartosc pola:\n");
    len = getline(&text, &len, stdin);
    text[len-1] = '\0'; //Remove newline
    tableWriteToField(x-1, y-1, text);
    free(text);
}
void readFieldFromTable()
{
    printf("Wpisz wierz i kolumne pola (numeracja od 1):\n");
    int x, y;
    scanf("%d %d", &y, &x);
    clearInputBuffer();
    char *text = tableReadFromField(x-1, y-1);
    if(!text)
    {
        printf("Bledne wspolrzedne pola!\n");
        return;
    }
    printf("|");
    printf("%s", text);
    printf("|\n");
    if(text) {free(text);}
}

void setTableAlignment()
{
    printf("Aktualne ustawienie wyrownania:\n|");
    for(int i=0; i<tableGetColNum(); i++)
    {
        printf("%c |", tableGetAlignment(i));
    }
    printf("\nWpisz nowe ustawienia:\n");
    for(int i=0; i<tableGetColNum(); i++)
    {
        char input;
        while((input = getchar()) == ' ');
        if(input == 'l' || input == 'c' || input == 'r') { tableSetAlignment(i, input); }
        //printf("#%c# \n", input);
    }
    clearInputBuffer();
}

void dumpStuffToTable()
{
    for(int j=0; j<tableGetColNum(); j++)
    {
        for(int i=0; i<tableGetRowNum(); i++)
        {
            char text[(tableGetRowNum()+tableGetColNum())+1];
            sprintf(text, "%dx%d", j+1, i+1);
            tableWriteToField(i, j, text);
        }
    }
}

void startMenu(int rows, int columns, int style)
{
    system("clear");
    createTable(rows, columns, style);
    rows = 0;
    columns = 0;
    style = 0;

    while(1)
    {
        printf("Co chcesz zrobic:\n"
                "(c) Utworzyc nowa tabele\n"
                "(e) Zmienic styl tabeli\n"
                "(a) Zmienic wyrownanie elementow\n"
                "(i) Wpisac tresc do pola\n"
                "(r) Wypisac tresc z pola\n"
                "(d) Wypelnic wszystkie pola przykladowym tekstem\n"
                "(p) Wypisac tabele na ekran\n"
                "(f) Zapisac tabele do pliku\n"
                "(q) Wyjsc z programu\n");
        printf("\nWybor: ");
        char input = getchar();
        clearInputBuffer();
        system("clear");
        switch (input)
        {
            case 'c':
                    createTable(rows, columns, style);
                    break;
            case 'e':
                    changeTableStyle(style);
                    break;
            case 'a':
                    setTableAlignment();
                    break;
            case 'i':
                    InsertFieldToTable();
                    break;
            case 'r':
                    readFieldFromTable();
                    break;
            case 'p':
                    printTableOnScreen();
                    break;
            case 'd':
                    printf("Wypelnienie wszystkich pol nadpisze aktualna zawartosc.\n"
                            "Kontynuowac? (t/N):\n");
                    char input = getchar();
                    clearInputBuffer();
                    if(input == 't' || input == 'T') { dumpStuffToTable(); }
                    break;
            case 'f':
                    printf("Wpisz nazwe pliku: ");
                    char *filename = 0;
                    size_t len;
                    len = getline(&filename, &len, stdin);
                    filename[len-1] = '\0'; //Remove newline
                    if(!writeTableToFile(filename))
                        printf("Nie udalo sie zapisac tabeli do pliku!\n");
                    else printf("Pomyslnie zapisano tabele do pliku\n");
                    break;
            case 'q':
                    return;
                    break;
            default:
                    printf("Bledna komenda\n");
                    break;
        }
    }
}

int main(int argc, const char **argv)
{
    if(argc == 3)
        startMenu(atoi(argv[2]), atoi(argv[1]), 0);
    else if(argc == 4)
        startMenu(atoi(argv[2]), atoi(argv[1]), atoi(argv[3]));
    else startMenu(0, 0, 0);
    return 0;
}
