#ifndef LTG_H
#define LTG_H
void deleteTable();
int makeNewTable(int rows, int columns, int style);
int tableGetRowNum();
int tableGetColNum();
int tableIsInBounds(int row, int column);
void tableWriteToField(int row, int column, const char *text);
char* tableReadFromField(int row, int column);
int tableGetFieldSize(int row, int column);
unsigned int tableLongestFieldSize(int column);
int tableGetStyle();
void tableSetStyle(int style);
char tableGetAlignment(int column);
void tableSetAlignment(int column, int value);
int tableExist();
int  writeTableToFile(const char* filename);
#endif
